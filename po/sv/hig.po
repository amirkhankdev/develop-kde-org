#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2023-08-01 14:40+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "KDE:s riktlinjer för mänskliga gränssnitt"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"**KDE:s riktlinjer för mänskliga gränssnitt (HIG)** ger konstruktörer och "
"utvecklare en mängd rekommendationer för att skapa vackra, användbara och "
"likformiga användargränssnitt för konvergerande skrivbordsprogram och "
"mobilapplikationer samt komponenter på arbetsytan. Vårt mål är att förbättra "
"användarupplevelsen genom att skapa gränssnitt för skrivbord, mobil och allt "
"däremellan som är likformiga, intuitiva och lättförståeliga."

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "Designvision"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr ""
"Vår designvision fokuserar på två egenskaper hos KDE:s programvara som "
"kopplar dess framtid till dess historia."

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Normalt enkelt, kraftfullt vid behov.](/hig/HIGDesignVisionFullBleed.png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "Normalt enkelt ..."

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""
"*Enkelt och inbjudande. KDE:s programvara är trevligt att uppleva och lätt "
"att använda.*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**Gör det enkelt att fokusera på det som är viktigt**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""
"Ta bort eller minimera element som inte är väsentliga för den primära "
"uppgiften eller huvuduppgiften. Använd mellanrum för att hålla saker och "
"ting organiserade. Använd färg för att få uppmärksamhet. Avslöja bara "
"ytterligare information eller valfria funktioner när det behövs."

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**\"Jag vet hur man gör det!\"**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""
"Gör det enklare att lära genom att återanvända designmönster från andra "
"program. Andra program som använder bra design och är lämpliga att följa."

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr "**Gör grovjobbet åt mig**"

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""
"Gör komplicerade uppgifter enkla. Låt nybörjare känna sig som experter. "
"Skapa sätt som gör att användare kan känna sig naturligt kraftfulla."

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "... kraftfullt vid behov"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Kraft och flexibilitet. KDE:s programvara låter användare enkelt vara "
"kreativa och effektivt produktiva.*"

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**Lös ett problem**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""
"Identifiera och gör helt klart för användaren vilket behov som uppfylls och "
"hur."

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr "**Ha alltid kontroll**"

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""
"Det ska alltid vara klart vad som kan göras, vad som för närvarande sker, "
"och vad som just har skett. Användaren ska aldrig vara i verktygets händer. "
"Ge användaren slutordet."

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**Var flexibel**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""
"Tillhandahåll vettiga förval men överväg valfria funktioner och "
"anpassningsalternativ som inte interfererar den primära uppgiften."

#: content/hig/_index.md:70
msgid "Note"
msgstr "Anmärkning"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE uppmuntrar att utveckla och konstruera för anpassningsbarhet, medan bra "
"standardinställningar tillhandahålls. Integrering med andra "
"skrivbordsmiljöer är också av godo, men i slutändan siktar vi på perfektion "
"i vår egen Plasma-skrivbordsmiljö med standardteman och inställningar. Det "
"målet får inte äventyras."
